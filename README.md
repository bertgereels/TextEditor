# TextEditor

## Information
TextEditor assignment for my algorithms course.

## What it has to do:
Build a simple text editor, which can:

Check if an HTML document has closing tags for all its opening tags. You may ignore single tags and attributes.
When editing an HTML document, provide autocompletion for closing tags.  You may ignore single tags and attributes.
Provide an undo/redo buffer of a fixed length, for instance undo/redo the last 50 events.

Predefined data structure classes may not be used.

## Bugs
Undo redo works fine for normal typing, but acts weird when auto tag completion is used.

## Sources
Emiel Estievenart
https://courses.cs.washington.edu/courses/cse143/12au/lectures/UndoRedoStack.java
http://www.java2s.com/Code/Java/Swing-JFC/Highlightofdiscontinousstring.htm
