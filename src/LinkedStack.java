public class LinkedStack<E> implements Stack<E>{
	
	private LinkedList<E> list;
	public static final int MaxSize = 50;
	
	public LinkedStack() {
		list = new LinkedList<>();
	}

	/**
	 * Returns stack size
	 */
	@Override
	public int size() {
		return list.getSize();
	}

	/**
	 * Returns true if stack is empty
	 */
	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}
	
	/**
	 * Automatically clears the entire stack using the pop method.
	 */
	public void clearStack() {
		while(!isEmpty()) {
			pop();
		}
	}

	/**
	 * Pushes an element onto the stack.
	 */
	@Override
	public void push(E element) {
		if(size() == MaxSize) {
			list.removeLast();
		}else if(size() < MaxSize) {
			list.prepend(element);
		}
	}

	/**
	 * Returns top of the stack
	 */
	@Override
	public E top() {
		if(isEmpty()) {
			return null;
		}
		return list.getHead();
	}

	/**
	 * Pops the stack and returns the popped element.
	 */
	@Override
	public E pop() {
		if(size() > 0) {
			return list.removeFirst();
		}
		return null;
	}

}
