import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Simple GUI for a text editor.
 *
 */
public class Texed extends JFrame implements DocumentListener, ActionListener, KeyListener{
	private JFrame frame;
	private JTextArea textArea;
	private JScrollPane scrollPane;
	private JPanel mainPnl;
	private JPanel textPnl;
	private JPanel buttonPnl;
	private JButton undoButton;
	private JButton redoButton;
	private JButton tagButton;
	private JLabel tagCounterDisplay;
	private LinkedStack<String> undoStack;
	private LinkedStack<String> redoStack;
	private String previousText;
	private char typedChar;

	private static final long serialVersionUID = 5514566716849599754L;
	/**
	 * Constructs a new GUI: A TextArea on a ScrollPane
	 */
	public Texed() {
		super();
		frame = new JFrame("Text Editor");
		mainPnl = new JPanel(new BorderLayout());
		textPnl = new JPanel();
		buttonPnl = new JPanel();
		
		undoButton = new JButton("Undo");
		undoButton.addActionListener(this);
		buttonPnl.add(undoButton);
		redoButton = new JButton("Redo");
		redoButton.addActionListener(this);
		buttonPnl.add(redoButton);
		tagButton = new JButton("Check Tags");
		tagButton.addActionListener(this);
		buttonPnl.add(tagButton);
		
		tagCounterDisplay = new JLabel();
		setTagLabelDefaultMessage();
		buttonPnl.add(tagCounterDisplay);
		
		textArea = new JTextArea(32, 70);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.getDocument().addDocumentListener(this);
		
		scrollPane = new JScrollPane(textArea);
		textPnl.add(scrollPane);

		mainPnl.setLayout(new BorderLayout());
		mainPnl.add(buttonPnl, BorderLayout.NORTH);
		mainPnl.add(textPnl, BorderLayout.SOUTH);
		
		frame.setSize(800,600);
		frame.setResizable(false);
		frame.add(mainPnl);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);	
		
		undoStack = new LinkedStack<>();
		redoStack = new LinkedStack<>();
		
		previousText = textArea.getText();
		undoStack.push(previousText);

		textArea.addKeyListener(this);
	}
	
	/**
	 * Entry point of the application: starts a GUI
	 */
	public static void main(String[] args) {
		new Texed();
	}
	
	
	/**
	 * ActionListener for the buttons
	 */
	 public void actionPerformed(ActionEvent e) {
		 if(e.getSource() == undoButton) {
			 undo();
		 }
		 if(e.getSource() == redoButton) {
			 redo();
		 }
		 if(e.getSource() == tagButton) {
			 HTMLHelper.checkTags(textArea, tagCounterDisplay);
		 }
	 }
	 
	 /**
	  * Undo method for the text in the textArea
	  */
	 public void undo() {
		 if(!undoStack.isEmpty()) {
			 String temp = undoStack.pop();
			 redoStack.push(textArea.getText());
			 textArea.setText(temp);
		 }
	 }
	 
	 /**
	  * Redo method for the text in the TextArea
	  */
	 public void redo() {
		 if(!redoStack.isEmpty()){
			 String temp = redoStack.pop();
			 undoStack.push(textArea.getText());
			 textArea.setText(temp);
		 }
	 }

	/**
	 * Callback when changing an element
	 */
	public void changedUpdate(DocumentEvent ev) {
	}

	/**
	 * Callback when deleting an element
	 */
	public void removeUpdate(DocumentEvent ev) {
	}

	/**
	 * Callback when inserting an element
	 * If the typed char is a space, in other words, detect when a new word is typed,
	 * add the text to the undoStack.
	 */
	public void insertUpdate(DocumentEvent ev) {
		//Check if the change is only a single character, otherwise return so it does not go in an infinite loop
		if(ev.getLength() != 1) return;
		System.out.println("Update detected");
		
		if(typedChar == ' ' ) {
			undoStack.push(textArea.getText());
			previousText = textArea.getText();
			redoStack.clearStack();
		}
		
		if(typedChar == '>') {
			SwingUtilities.invokeLater(new Task(HTMLHelper.addTag(textArea)));
		}
	}
	
	/**
	 * Detect when a character is typed and store the typed char
	 */
	@Override
	public void keyPressed(KeyEvent arg0) {
		typedChar = arg0.getKeyChar();
		setTagLabelDefaultMessage();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub	
	}
	
	/**
	 * Set the default message for the label displaying the counter of the open tags
	 */
	public void setTagLabelDefaultMessage() {
		tagCounterDisplay.setText("Click 'Check Tags' button to check for open tags.");	
	}


	/**
	 * Runnable: change UI elements as a result of a callback
	 * Start a new Task by invoking it through SwingUtilities.invokeLater
	 */
	private class Task implements Runnable {
		private String text;
		
		/**
		 * Pass parameters in the Runnable constructor to pass data from the callback 
		 * @param text which will be appended with every character
		 */
		Task(String text) {
			this.text = text;
		}

		/**
		 * The entry point of the runnable
		 */
		public void run() {
			textArea.append(text);
			undoStack.push(text);
		}
	}
}