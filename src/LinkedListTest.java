import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LinkedListTest {
	
	private LinkedList<String> list = new LinkedList<String>();
	
	@Test
	public void testIsEmpty() {	
		assertEquals(true,list.isEmpty());
		list.prepend("Hallo");
		assertEquals(false,list.isEmpty());
	}	
	
	@Test
	public void testSize() {
		assertEquals(0,list.getSize());
		list.prepend("Hallo");
		assertEquals(1,list.getSize());
	}

	@Test
	public void testGetHead() {
		list.prepend("Hallo");
		list.prepend("Goedendag");
		assertEquals("Goedendag",list.getHead());
	}
	
	@Test 
	public void testRemoveLast() {
		list.prepend("Hallo");
		list.prepend("Goedendag");
		assertEquals(2,list.getSize());
		list.removeLast();
		assertEquals(1,list.getSize());
	}
	
	@Test 
	public void testRemoveFirst() {
		list.prepend("Hallo");
		list.prepend("Goedendag");
		assertEquals(2,list.getSize());
		assertEquals("Goedendag",list.getHead());
		assertEquals("Goedendag",list.removeFirst());
		assertEquals(1,list.getSize());
		assertEquals("Hallo",list.getHead());
	}
}
