import static org.junit.Assert.*;
import org.junit.Test;

public class LinkedStackTest {

	private LinkedStack<String> stack = new LinkedStack<String>();
	
	@Test
	public void testIsEmpty() {	
		assertEquals(true,stack.isEmpty());
		stack.push("Hallo");
		assertEquals(false,stack.isEmpty());
	}	

	@Test
	public void testSize() {
		assertEquals(0,stack.size());
		stack.push("Hallo");
		assertEquals(1,stack.size());
	}
	

	@Test
	public void testTop() {
		stack.push("Hallo");
		assertEquals("Hallo",stack.top());
		assertEquals(1,stack.size());
	}

	@Test
	public void testPushAndPop() {
		stack.push("Hallo");
		assertEquals("Hallo", stack.pop());
		
		stack.push("Hallo1");
		stack.push("Hallo2");
		
		assertEquals("Hallo2",stack.pop());
		assertEquals("Hallo1",stack.pop());
		assertEquals(0,stack.size());	
	}

}