import javax.swing.JLabel;
import javax.swing.JTextArea;

public class HTMLHelper {
	
	public HTMLHelper(){
		
	}
	
	/**
	 * Automatically adds the appropriate closing tag.
	 * This method is called each time a '>' character is typed.
	 * @param textArea
	 * @return the tag to be added.
	 */
	public static String addTag(JTextArea textArea) {
		String currentText = textArea.getText();
		String tagType =  currentText.substring(currentText.lastIndexOf("<") + 1, currentText.lastIndexOf(">"));
		String returnTag = "</" + tagType +">";
		return returnTag;
	}
	
	/**
	 * Executes when 'check tags' button is pressed.
	 * This method counts the open and closed tags and determines if there are any open tags left.
	 * @param textArea
	 * @param tagCounterDisplay
	 */
	public static void checkTags(JTextArea textArea, JLabel tagCounterDisplay) {
		int openTagCounter = 0;
		String currentText = textArea.getText();
		for(int i = 0; i <= currentText.length()-1; i++) {
			if(currentText.charAt(i) == '<' && (currentText.charAt(i+1) != '/') && (currentText.charAt(i+2) == '>')) {
				openTagCounter++;
			}
			if(currentText.charAt(i) == '<' && (currentText.charAt(i+1) == '/') && (currentText.charAt(i+3) == '>')) {
				openTagCounter--;
			}
		}
		tagCounterDisplay.setText("There are/is: " + openTagCounter + " open tag(s).");	
	}
	
}
